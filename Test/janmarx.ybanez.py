print("Hello World!")
print("What up dawg?")
print("Janmarx")
print(2 + 3)
print("your new score is,", 1030 + 10)
print("score:", "10+3")
print("The file is stored in C:\new folder")
print("The file is stored in C:\\new folder")
print("This\nis\nmy\nsample.")
print("\t this is a indentation.")
print("\r\n Carriage return and line down.")
# this is a comment and will be ignored
x = 10
print(x)  # This prints out 10
print("x")  # This only prints out x
print("x=", 10)

x = 5
x = x + 1
print(x)
print()
print()

# Chapter 3
a = 4
b = 5
if a <= b:
    print("a is less than and equal to b")
if a >= b:
    print("a is greater than and equal to b")
print("done")
print()
a = 1
b = 3
if a == 1:
    print("a equals 1")
    print()
if b == 2:
    print("good job")
    print()
if b == 3:
    print("Nice you da best")
print("done")
print()
# And/Or
a = 4
b = 6
c = 9
if a > b or a < c:
    print("good job")
if a > b or a > c:
    print("Nice")
print("done")

# Boolean variables
a = 1
b = 0
if not 0:
    print("hello")
print("done")

a = 3
b = 3
c = a == b
print(c)
score = 0

a = 50
b = 20
d = a - b
print(d)
