print("Pink", end=" ")
print("Octopus")
print()
# Advance looping problems:
# 1.
for row in range(10):
    print("*", end=" ")
print()
# 2.
for row in range(10):
    print("*", end=" ")
print()
for row in range(5):
    print("*", end=" ")
print()
for row in range(20):
    print("*", end=" ")
print()
print()

# 3.
for row in range(10):
    for columns in range(10):
        print("*", end=" ")
    print()
print()

# 4.
for row in range(5):
    for columns in range(20):
        print("*", end=" ")
    print()
print()

# 8.
for i in range(10):
    for j in range(i + 1):
        print(j, end=" ")
    print()
print()

