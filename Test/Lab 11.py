import pygame

# Define some colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Call this function so the Pygame library can initialize itself
pygame.init()

# Create an 800x600 sized screen
screen = pygame.display.set_mode([700, 500])

# This sets the name of the window
pygame.display.set_caption('CMSC 150 is cool')

done = False

clock = pygame.time.Clock()

# Load and set up graphics.
background_image = pygame.image.load("saturn_family1.jpg").convert()
player_image = pygame.image.load("player.png").convert()

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    player_position = pygame.mouse.get_pos()
    x = player_position[0]
    y = player_position[1]

    screen.blit(background_image, [0, 0])
    screen.blit(player_image, [x, y])
    player_image.set_colorkey(BLACK)

    pygame.display.flip()

    clock.tick(60)

pygame.quit()