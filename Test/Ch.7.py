# Copy of the array to modify
my_list = [5, 76, 8, 5, 3, 3, 56, 5, 23]

# Loop from 0 up to the number of elements
# in the array:
for i in range(len(my_list)):
    # Modify the element by doubling it
    my_list[i] = my_list[i] * 2

# Print the result
print(my_list)

x = "This is a sample string"
# x = "0123456789"

print("x=", x)

# Accessing a single character
print("x[0]=", x[0])
print("x[1]=", x[1])

# Accessing from the right side
print("x[-1]=", x[-1])

# Access 0-5
print("x[:6]=", x[:6])
# Access 6
print("x[6:]=", x[6:])
# Access 6-8
print("x[6:9]=", x[6:9])
print()
plain_text = "This is a test. ABC abc"

for c in plain_text:
    print(c, end="")

plain_text = "This is a test. ABC abc"

for c in plain_text:
    print(ord(c), end=" ")

plain_text = "This is a test. ABC abc"

for c in plain_text:
    x = ord(c)
    x = x + 1
    c2 = chr(x)
    print(c2, end="")
print()

for character in "This is a test.":
    print(character, end="")
print()

# n = int(input("Enter a month number: "))
# months = "JanFebMarAprMayJunJulAugSepOctNovDec"
# for n in months:
    # userInput = int(input("Enter a month number: "))
    # print(months[0:2])

months = "JanFebMarAprMayJunJulAugSepOctNovDec"

n = int(input("Enter a month number: "))
if n == 1:
    print(months[0:3])
elif n == 2:
    print(months[3:6])
elif n == 3:
    print(months[6:9])
elif n == 4:
    print(months[9:12])
elif n == 5:
    print(months[12:15])
elif n == 6:
    print(months[15: 18])
elif n == 7:
    print(months[18:21])
elif n == 8:
    print(months[21:24])
elif n == 9:
    print(months[24:27])
elif n == 10:
    print(months[27:30])
elif n == 11:
    print(months[30:33])
elif n == 12:
    print(months[33:36])
