import random
# Vinay Sabu
# Janmarx Ybanez
# Chapter 4 lab
# camel game

# Instructions
print("")
print("                   ----------WELCOME TO INFINITY RUN----------")
print("Congratulations!!! You are Iron-Man and you have just stolen the power stone from The Mad Titan,")
print("Thanos. And you have to get from Wakanda to the Avengers Headquarters in New York, but Thanos")
print("is chasing you down. Survive the long journey without letting the strongest being in the")
print("whole universe catch you. If he does he'll get the power stone and destroy half of all life.")
print("Avenger's Headquarters is 700 km away. Get there to keep the stone away from Thanos.\n")


# Variables
arcReactorPower = 0
tiredness = 0
kmTraveled = 50
thanos = 20
# Variable below is distance between Iron man and Thanos
distanceBetween = kmTraveled - thanos

# Boolean variable
done = False
# Prints out the Options
while not done:
    print("A. Engage Thrusters at full speed\nS. Moderated speed to save power\nD. Shoot at Thanos to slow him down")
    print("W. Stop and Rest to recharge\nE. Status Check\nQ. Give Stones to Thanos")
    userChoice = input("Select a letter.")

# Option Q: Quit Game
    if userChoice.upper() == "Q":
        print("")
        done = True
        print("_________________________________")
        print("     GAME OVER\n")

# Option A: Full speed to New York
    if userChoice.upper() == "A":
        print("")
        arcReactorPower += random.randrange(21, 26)
        tiredness += random.randrange(20, 28)
        kmTraveled += random.randrange(39, 48)
        thanos += random.randrange(20, 33)
        print(" You've traveled", kmTraveled, "Km")
        distanceBetween = kmTraveled - thanos
        print(" Thanos is", distanceBetween, "Km behind\n")

# Option S: Moderate speed
    elif userChoice.upper() == "S":
        print("")
        arcReactorPower += random.randrange(14, 19)
        tiredness += random.randrange(12, 18)
        kmTraveled += random.randrange(20, 29)
        thanos += random.randrange(14, 20)
        print(" You've traveled", kmTraveled, "Km")
        distanceBetween = kmTraveled - thanos
        print(" Thanos is", distanceBetween, "Km behind\n")

# Option D: Shoot at Thanos to slow him down
    elif userChoice.upper() == "D":
        print("")
        arcReactorPower += random.randrange(18, 22)
        tiredness += random.randrange(17, 22)
        kmTraveled += random.randrange(0, 11)
        thanos -= random.randrange(5, 18)
        print(" You've traveled", kmTraveled, "Km")
        distanceBetween = kmTraveled - thanos
        print(" Thanos is", distanceBetween, "Km behind\n")

# Option W: Rest
    elif userChoice.upper() == "W":
        print("")
        arcReactorPower -= random.randrange(20, 30)
        tiredness -= random.randrange(17, 29)
        thanos += random.randrange(13, 20)
        print(" You've traveled", kmTraveled, "Km")
        distanceBetween = kmTraveled - thanos
        print(" Thanos is", distanceBetween, "Km behind\n")

# Option E: Status Check
    elif userChoice.upper() == "E":
        print("")
        print(" ArcReactor Power = ", arcReactorPower, "%")
        print(" Tiredness = ", tiredness)
        print(" Traveled = ", kmTraveled, "Km")
        distanceBetween = kmTraveled - thanos
        print(" Thanos is", distanceBetween, "Km behind\n")

# Random event where Thanos' companions appears
    if kmTraveled >= 500 or kmTraveled <= 600:
        randomPossibility = random.randrange(1, 6)
        if randomPossibility == 1:
            print("The Black Order appeared out of nowhere.\nThey are helping Thanos catch you.\n")
            print("They are blocking you way. What are you going to do?")
            print("F. Fight Them\nG. Fly past them.")
            userChoice = input("Think fast! ")
            if userChoice.upper() == "F":
                arcReactorPower += random.randrange(18, 27)
                tiredness += random.randrange(19, 24)
                thanos += random.randrange(10, 15)
                blackOrder = False
                while not blackOrder:
                    print("You shot all of the enemies down. But it took a huge amount of energy.")
                    distanceBetween = kmTraveled - thanos
                    print("Thanos, is ", distanceBetween, "km behind you.\n")
                    blackOrder = True
            elif userChoice.upper() == "G":
                arcReactorPower += random.randrange(15, 28)
                tiredness += random.randrange(10, 15)
                thanos += random.randrange(5, 10)
                blackOrder = False
                while not blackOrder:
                    print("You successfully outran them. But it took up a huge amount of energy.")
                    distanceBetween = kmTraveled - thanos
                    print("Thanos, is ", distanceBetween, "km behind you.\n")
                    blackOrder = True

# Game Over notifications/conditions

# Alert if Thanos gets too close
    if distanceBetween <= 29:
        print(" Thanos is getting close to you")
        print("_________________________________")

# Alert if energy is close to 100
    elif arcReactorPower >= 75:
        print(" Your ArcReactor Power is almost reaching 100")
        print("_________________________________")

# Alert if tiredness is close to 100
    elif tiredness >= 80:
        print(" You are almost going to collapse from tiredness")
        print("_________________________________")

# Alert if Iron Man is getting close to Headquarters
    elif kmTraveled >= 600:
        print(" You're", 700 - kmTraveled, "Km from the Avengers Headquarters")
        print("_________________________________")

# Loses game if Thanos caught up
    if thanos >= kmTraveled:
        print("_________________________________")
        print("     Thanos caught up to you. GAME OVER!\n")
        done = True

# Loses game if Tiredness reaches 100
    if tiredness >= 100:
        print("_________________________________")
        print("     You collapsed from tiredness. GAME OVER!\n")
        done = True

# Loses game if energy is 100
    if not done and arcReactorPower >= 100:
        print("_________________________________")
        print("     Your ArcReactor Power is 100. GAME OVER!\n")
        done = True

# Wins the game if Iron Man has reached 700 km
    if kmTraveled >= 700:
        print("_________________________________")
        print("     You reached New York. You Win!")
        done = True

# limits so that none of the variables below will not be negative:
    if arcReactorPower < 0:
        arcReactorPower = 0
    if tiredness < 0:
        tiredness = 0
    if thanos <= 0:
        thanos = 0
    if kmTraveled < 0:
        kmTraveled = 0
    if kmTraveled > 700:
        kmTraveled = 700
