import random
# Vinay Sabu
# Janmarx Ybanez
# Chapter 4 lab
# camel game
# Instructions

print("")
print("                   ----------WELCOME TO INFINITY RUN----------")
print("Congratulations!!! You are Iron-Man and you have just stolen the power stone from The Mad Titan,")
print("Thanos. And you have to get from Wakanda to the Avengers Headquarters in New York, but Thanos")
print("is chasing you down. Survive the long journey without letting the strongest being in the")
print("whole universe catch you. If he does He'll get the power stone and destroy half of all life.")
print("Avenger's Headquarters is 3300 km away. Get there to keep the stone away from Thanos.\n")

# Variables
kilometers_traveled = 0
arc_reactor_power = 75
tiredness = 50
thanos = 50
total_distance = 2600

# variable to quit
done = False
while not done:
    print(" A. Take a break to recharge\n B. Engage thrusters to full speed\n C. Shoot at Thanos to slow him down")
    print(" D. Slow down to save power\n E. Status Check\n Q. Give stone to Thanos  ")
    user_choice = input("What's the move? ")
    print("")
# option Q Quit
    if user_choice.upper() == "Q":
        done = True
        print("Thanos Snapped and half of all life no longer exists.")
        print("You lose.\n")
        print("_________________________________")
        print("\n           GAME OVER!            ")
        print("_________________________________")

# option A: take a break
    elif user_choice.upper() == "A":
        arc_reactor_power = arc_reactor_power + random.randrange(15, 26)
        tiredness = tiredness - random.randrange(10, 21)
        thanos = thanos - random.randrange(20, 31)
        if thanos < 0:
            thanos = 0
        print("Thanos is", thanos, "km behind you.\n")

# option B: Engage thrusters to full speed
    elif user_choice.upper() == "B":
        arc_reactor_power = arc_reactor_power - random.randrange(20, 25)
        tiredness = tiredness + random.randrange(15, 21)
        kilometers_traveled = random.randrange(200, 251)
        thanos = thanos + random.randrange(10, 16)
        total_distance = total_distance + kilometers_traveled
        print("You traveled", kilometers_traveled, "km.")
        if thanos < 0:
            thanos = 0
        print("Thanos is", thanos, "km behind you.\n")

# option C: shoot at thanos
    elif user_choice.upper() == "C":
        kilometers_traveled = random.randrange(150, 201)
        arc_reactor_power = arc_reactor_power - random.randrange(0, 6)
        tiredness = tiredness + random.randrange(5)
        thanos = thanos + random.randrange(10, 16)
        total_distance = total_distance + kilometers_traveled
        print("You traveled", kilometers_traveled, "km.")
        if thanos < 0:
            thanos = 0
        print("Thanos is", thanos, "km behind you.\n")

# option D: slow down to save power
    elif user_choice.upper() == "D":
        arc_reactor_power = arc_reactor_power - random.randrange(0, 5)
        tiredness = tiredness - random.randrange(5)
        kilometers_traveled = random.randrange(100, 151)
        thanos = thanos - random.randrange(10, 16)
        total_distance = total_distance + kilometers_traveled
        print("You traveled", kilometers_traveled, "km.")
        if thanos < 0:
            thanos = 0
        print("Thanos is", thanos, "km behind you.\n")

#  option E: status check
    elif user_choice.upper() == "E":
        print("ArcReactor Power:", arc_reactor_power, "%")
        print("Tiredness:", tiredness)
        print("Kilometers Traveled:", kilometers_traveled, "km")
        print("Distance to New York:", 3300 - total_distance, "km")
        if thanos < 0:
            thanos = 0
        print("Thanos, is ", thanos, "km behind you.\n")

    # negative Random event
    if total_distance <= 2500:
        random_possibility = random.randrange(1, 6)
        if random_possibility == 1:
            print("The Black Order appeared out of nowhere.\nThey are helping Thanos catch you.\n")
            print("They are blocking you way. What are you going to do?")
            print("F. Fight Them\nG. Fly past them.")
            user_choice = input("Think fast! ")
            if user_choice.upper() == "F":
                arc_reactor_power = arc_reactor_power - random.randrange(20, 30)
                tiredness = tiredness - random.randrange(20, 25)
                thanos = thanos - random.randrange(10, 15)
                black_order = False
                while not black_order:
                    print("You shot all of the enemies down. But it took a huge amount of energy.")
                    print("Thanos, is ", thanos, "km behind you.\n")
                    black_order = True
            elif user_choice.upper() == "G":
                arc_reactor_power = arc_reactor_power - random.randrange(20, 35)
                tiredness = tiredness - random.randrange(10, 15)
                thanos = thanos - random.randrange(5, 10)
                black_order = False
                while not black_order:
                    print("You successfully outran them. But it took up a huge amount of energy.")
                    print("Thanos, is ", thanos, "km behind you.\n")
                    black_order = True

# game over conditions
# power level 0 / game over notification
    if arc_reactor_power > 100:
        arc_reactor_power = 100
    if arc_reactor_power <= 30:
        print("Arc Reactor Power is low.\n ")
    if arc_reactor_power <= 0:
        print("The suit ran out of power and shut down.\nThanos got the stone. Half of the universe was decimated.\n")
        print("_________________________________")
        print("\n           GAME OVER!            ")
        print("_________________________________")
        done = True


# collapsed from tiredness / game over notification
    if tiredness < 0:
        tiredness = 0
    if tiredness >= 80:
        print("You are getting tired.\n")
    if tiredness >= 100:
        print("You collapsed from tiredness.\nThanos got the stone. Half of the universe was decimated.\n")
        print("_________________________________")
        print("\n           GAME OVER!            ")
        print("_________________________________")
        done = True
    elif tiredness <= 10:
        print("You are well rested.\n")

# Thanos is close alert
    if thanos < 0:
        thanos = 0
    if thanos <= 20:
        print("Thanos is getting close to you.\n")
# Thanos caught up / game over notification
    if thanos <= 0:
        print("Thanos caught up to you,\nThanos got the stone from you.\nHalf of the universe was decimated.\n")
        print("_________________________________")
        print("\n           GAME OVER!            ")
        print("_________________________________")
        done = True

# almost at wakanda alert
    if total_distance > 3300:
        total_distance = 3300
    if total_distance > 3000:
        print("You are ", 3300 - total_distance, "km away from The Avengers Headquarters.\n")
# reached wakanda notification
    if total_distance == 3300:
        print("\n\nYou made it to the AVENGERS Headquarters.\nThanos cannot get the power stone.\n")
        print("YOU SAVED THE WHOLE UNIVERSE!!! \n")
        print("_________________________________")
        print("\n           GAME OVER!            ")
        print("_________________________________")
        done = True
