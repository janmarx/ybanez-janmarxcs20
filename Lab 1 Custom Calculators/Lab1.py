# Part A - Temperature Conversions

# This program converts the temperature in fahrenheit to celsius
print("Enter the temperature in fahrenheit and it will convert to celsius")
print()
# The temperature in Fahrenheit is entered
fahrenheit = float(input("Enter temperature in Fahrenheit:"))
print()
print()
# The temperature in Fahrenheit is converted to celsius using the formula below
celsius = (fahrenheit - 32) * 5 / 9

# Output
print("The temperature in celsius:", celsius)

