# Part B - Area of Trapezoid

# This program calculates the area of a trapezoid
print("To find the area of a trapezoid enter the height of the trapezoid, length of the bottom base, and length of the top base:")
print()

# The height of the trapezoid is entered
heightOfTrapezoid = float(input("height of trapezoid:"))

# The length of bottom base is entered
baseOfTrapezoid1 = float(input("length of the bottom base:"))

# The length of the top base is entered
baseOfTrapezoid = float(input("length of the top base:"))

# All the numbers you have entered will be put into this formula to calculate the area of the trapezoid
area = .5 * (baseOfTrapezoid1 + baseOfTrapezoid) * heightOfTrapezoid

print()
print()
# Output
print("The area of the trapezoid:", area)
