# Part C - Area of Circle

# This program calculates the area of a circle
print("To find the area of a circle enter the radius:")
print()

# The radius of the circle is entered
radius = float(input("radius of the circle:"))

# This is the formula used to find the area of a circle
area = 3.14159 * radius ** 2

print()
print()
# Output
print("the area of the circle:", area)
