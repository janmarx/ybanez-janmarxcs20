# lab 9
# Functions

import random
# Question 1
print("Question #1")


def min3(a, b, c,):
    if a <= b and a <= c:
        return a
    elif b <= a and b <= c:
        return b
    elif c <= a and c <= b:
        return c


print(min3(4, 7, 5))
print(min3(4, 5, 5))
print(min3(4, 4, 4))
print(min3(-2, -6, -100))
print(min3("Z", "B", "A"))
print()


# Question 2
print("Question #2")


def box(x, y):
    for row in range(x):
        for column in range(y):
            print("*", end="")
        print()
    return x, y


box(7, 5)  # Print a box 7 high, 5 across
print()   # Blank line
box(3, 2)  # Print a box 3 high, 2 across
print()   # Blank line
box(3, 10)  # Print a box 3 high, 10 across
print()

# Question 3
print("Question #3")

my_list = [36, 31, 79, 96, 36, 91, 77, 33, 19, 3, 34, 12, 70, 12, 54, 98, 86, 11, 17, 17]


def find(list_number, key):
    for i in range(len(list_number)):
        if list_number[i] == key:
            print("Found", key, " at position", i)


find(my_list, 12)
find(my_list, 91)
find(my_list, 80)
print()

# question b of 4

# question 4
print("Question #4")
print("A ")
random_list = []


def create_list(x):
    for x in range(1, 6):
        x = random.randrange(1, 6)
        random_list.append(x)

    return random_list


my_list = create_list(5)
print(my_list)
print()
print("B")


def count_list(list_count, add):
    for i in range(len(list_count)):
        if list_count[i] == add:
            list_count += 1

        return add


count = count_list([1, 2, 3, 3, 3, 4, 2, 1], 3)
print(count)
print()

print("C ")


def average_list(numbers):
    average = 0
    for i in range(len(numbers)):
        average += numbers[i]
    average /= len(numbers)
    return average


avg = average_list([1, 2, 3])
print(avg)

print()
print("D")
