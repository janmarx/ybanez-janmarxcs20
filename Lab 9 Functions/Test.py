# Calculates the volume of a cylinder
def volumeCylinder(radius, height):
    pi = 3.14159265359
    area = pi * radius ** 2
    volume = area * height
    return volume

def volume2(radius, height):
    return 3.14159265359 * radius ** 2 * height


print(volumeCylinder(1, 10))
print(volume2(1, 10))

volumeCylinder(234, 43)



