"""
Final Project
Etch a Sketch
"""

import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GRAY = (128, 128, 128)

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Testing")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

"""
-Variables to draw
-Sets X and Y speeds to zero
-X and Y Coordinates
"""
drawList = []
color = BLACK
xSpeed = 0
ySpeed = 0
pencilKeyboardX = 640
pencilKeyboardY = 360


# Function for pencil to draw circles
def pencil(screen, xCoordinate, yCoordinate):
    pygame.draw.circle(screen, color, [110 + xCoordinate - 100, 110 + yCoordinate - 100], 2)


# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:

            # Clears the screen
            if event.key == pygame.K_SPACE:
                drawList.clear()

            # Changes X and Y speeds
            if event.key == pygame.K_LEFT:
                xSpeed = -1
            if event.key == pygame.K_RIGHT:
                xSpeed = 1
            elif event.key == pygame.K_UP:
                ySpeed = -1
            elif event.key == pygame.K_DOWN:
                ySpeed = 1

            # Changes the color of the pencil
            elif event.key == pygame.K_1:
                color = BLACK
            elif event.key == pygame.K_2:
                color = GREEN
            elif event.key == pygame.K_3:
                color = RED
            elif event.key == pygame.K_4:
                color = BLUE

            # Changes the X and Y speeds back to zero
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or pygame.K_RIGHT or pygame.K_UP or pygame.K_DOWN:
                xSpeed = 0
                ySpeed = 0

    # --- Game logic should go here

    # Makes the pencil move to draw
    pencilKeyboardX += xSpeed
    pencilKeyboardY += ySpeed

    # Draws the pencil
    pencil(screen, pencilKeyboardX, pencilKeyboardY)
    drawList.append([pencilKeyboardX, pencilKeyboardY])

    # If you want a background image, replace this clear with blit'ing the
    # background image
    screen.fill(WHITE)

    # --- Drawing code should go here

    # Draws a bunch of circles when the X and Y Coordinates changes
    for pencilKeyboardX, pencilKeyboardY in drawList:
        pygame.draw.circle(screen, color, [pencilKeyboardX, pencilKeyboardY], 2)

    # Create boundaries so that x and y doesn't go off screen
    if pencilKeyboardX <= 0:
        pencilKeyboardX = 0
    if pencilKeyboardX >= 1280:
        pencilKeyboardX = 1280
    if pencilKeyboardY <= 0:
        pencilKeyboardY = 0
    if pencilKeyboardY >= 720:
        pencilKeyboardY = 720

    # Draws instructions to play Etch a Sketch
    font = pygame.font.SysFont('Calibri', 15, True, False)
    text = font.render("Etch A Sketch", True, GRAY)
    screen.blit(text, [4, 0])
    text = font.render("____________", True, GRAY)
    screen.blit(text, [4, 1])
    text = font.render("Press space to clear screen", True, GRAY)
    screen.blit(text, [4, 23])
    text = font.render("Press arrow keys to draw", True, GRAY)
    screen.blit(text, [4, 46])
    text = font.render("Press keys to change color:", True, GRAY)
    screen.blit(text, [4, 69])
    text = font.render("Black(1), Green(2), Red(3), Blue(4)", True, GRAY)
    screen.blit(text, [4, 92])

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
