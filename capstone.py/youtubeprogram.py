"""
Test of etch a sketch
"""

import pygame, sys
from pygame.locals import *

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GRAY = (128, 128, 128)

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Youtube Program")

# Loop until the user clicks the close button.
done = False

x = 640
y = 360

color = BLACK

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
screen.fill(GRAY)

# -------- Main Program Loop -----------
while 1:
    clock.tick(60)
    pygame.draw.circle(screen, color, [x, y], 2)
    pygame.display.flip()
    key = pygame.key.get_pressed()
    if key[pygame.K_RIGHT]: x += 1
    if key[pygame.K_LEFT]: x -= 1
    if key[pygame.K_DOWN]: y += 1
    if key[pygame.K_UP]: y -= 1

    # Create boundaries so that x and y doesn't go off screen
    if x <= 0:
        x = 0
    if x >= 1280:
        x = 1280
    if y <= 0:
        y = 0
    if y >= 720:
        y = 720

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        # Different keys change colo rs
        elif event.type == KEYDOWN and event.key == K_ESCAPE:
            sys.exit()
        elif event.type == KEYDOWN and event.key == K_SPACE:
            screen.fill(GRAY)
        elif event.type == KEYDOWN and event.key == K_1:
            color = BLACK
        elif event.type == KEYDOWN and event.key == K_2:
            color = GREEN
        elif event.type == KEYDOWN and event.key == K_3:
            color = RED
        elif event.type == KEYDOWN and event.key == K_4:
            color = BLUE
