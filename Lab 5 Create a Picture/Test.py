"""
 Ch.5 Pygame Testing
"""

import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
CRIMSON = (220, 20, 20)
CHERRY_BLOSSOM = (255, 183, 197)
RED_PLUM = (126, 49, 77)
PURPLE = (158, 66, 244)

PI = 3.141592653

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Test")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(CHERRY_BLOSSOM)

    # --- Drawing code should go here
    # This is the rim
    pygame.draw.ellipse(screen, CRIMSON, [900, 200, 250, 80], 10)
    # This is the Basketball
    pygame.draw.circle(screen, RED_PLUM, [500, 200], 75)
    # This is Kyrie's logo (K)
    pygame.draw.rect(screen, BLACK, [600, 360, 50, 300], 20)
    pygame.draw.rect(screen, BLACK, [515, 360, 50, 300], 20)
    pygame.draw.rect(screen, GREEN, [600, 370, 26, 280])
    pygame.draw.rect(screen, GREEN, [515, 370, 26, 280])
    pygame.draw.rect(screen, PURPLE, [535, 370, 23, 280])
    pygame.draw.rect(screen, PURPLE, [620, 370, 23, 280])
    # Lines behind the Basketball
    pygame.draw.arc(screen, BLACK, [308, 160, 200, 100], PI / 2, PI, 4)
    pygame.draw.arc(screen, BLACK, [308, 200, 200, 100], PI / 2, PI, 4)
    pygame.draw.arc(screen, BLACK, [310, 230, 180, 90], PI / 2, PI, 4)

    # Select the font to use, size, bold, italics
    font = pygame.font.SysFont('Calibri', 50, True, False)

    # Render the text. "True" means anti-aliased text.
    # Black is the color. This creates an image of the
    # letters, but does not put it on the screen
    text = font.render("Kyrie Irving Logo", True, BLACK)

    # Put the image of the text on the screen at 250x250
    screen.blit(text, [900, 660])

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
