# Janmarx Ybanez
# Lab 5
# Kyrie Irving Logo
# ComSci 20
# 08/ 03/ 19

import pygame

# Define some colors
GREEN = (0, 255, 0)
CHERRY_BLOSSOM = (255, 183, 197)
PURPLE = (158, 66, 244)
SILVER = (192, 192, 192)
INDIGO = (75, 0, 130)

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Lab 5. Kyrie Irving Logo")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # Background for my drawing
    screen.fill(INDIGO)

    # --- Drawing code should go here
    # Draws out a circle and a polygon for like the background of Kyrie's logo
    pygame.draw.circle(screen, CHERRY_BLOSSOM, [600, 340], 330)
    pygame.draw.polygon(screen, PURPLE, [[400, 80], [800, 80], [800, 600], [400, 600]])

    # Draws the Kyrie Irving logo
    pygame.draw.polygon(screen, GREEN, [[560, 110], [560, 300], [590, 280], [590, 110]])
    pygame.draw.polygon(screen, GREEN, [[600, 110], [630, 110], [630, 260], [600, 280]])
    pygame.draw.polygon(screen, GREEN, [[520, 130], [520, 490], [550, 490], [550, 130]])
    pygame.draw.line(screen, GREEN, [550, 340], [669, 270], 34)
    pygame.draw.polygon(screen, GREEN, [[560, 510], [590, 510], [590, 345], [560, 365]])
    pygame.draw.polygon(screen, GREEN, [[600, 510], [600, 340], [630, 350], [630, 510]])
    pygame.draw.rect(screen, GREEN, [640, 130, 30, 140])
    pygame.draw.rect(screen, GREEN, [640, 340, 30, 150])
    pygame.draw.line(screen, GREEN, [669, 340], [600, 310], 32)

    # Draws the name, "KYRIE"
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("KYRIE", True, PURPLE)
    screen.blit(text, [155, 300])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("KYRIE", True, SILVER)
    screen.blit(text, [930, 300])

    # Draws the name, "IRVING"
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("IRV", True, PURPLE)
    screen.blit(text, [527, 680])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("ING", True, SILVER)
    screen.blit(text, [600, 680])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("ING", True, SILVER)
    screen.blit(text, [600, 3])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("IRV", True, PURPLE)
    screen.blit(text, [523, 3])

    # --- Updates the screen with what I've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
