import pygame
import random

# Define Colours
GREEN = (0, 255, 0)
CHERRY_BLOSSOM = (255, 183, 197)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
PURPLE = (158, 66, 244)
RED_PLUM = (126, 49, 77)
SILVER = (192, 192, 192)
HOT_PINK = (255, 105, 180)
INDIGO = (75, 0, 130)
BLUE = (0, 0, 255)

pygame.init()

size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Ch. 5")

done = False

clock = pygame.time.Clock()

starlist = []
for star in range(15, 200):
    x = random.randrange(0, 1280)
    y = random.randrange(0, 720)
    starlist.append((x, y))

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    screen.fill(INDIGO)

    for star in starlist:
        pygame.draw.circle(screen, BLUE, star, random.randrange(3, 5))

    # for i in range(0, 100, 10):
        # stars = random.randrange(0, 100, 10)
        # pygame.draw.circle(screen, BLUE, [200, 300 + stars], 3 + 2)

    # --- Game logic should go here

    mousePosition = pygame.mouse.get_pos()
    mouseX = mousePosition[0]
    mouseY = mousePosition[1]

    print("(" + str(mouseX) + ", " + str(mouseY) + ")")

    # Background for my drawing
    # screen.fill(INDIGO)

    # Draws out a circle and a polygon for like the background of Kyrie's logo
    pygame.draw.circle(screen, CHERRY_BLOSSOM, [600, 340], 330)
    pygame.draw.polygon(screen, PURPLE, [[400, 80], [800, 80], [800, 600], [400, 600]])

    # Draws the Kyrie Irving logo
    pygame.draw.polygon(screen, GREEN, [[560, 110], [560, 300], [590, 280], [590, 110]])
    pygame.draw.polygon(screen, GREEN, [[600, 110], [630, 110], [630, 260], [600, 280]])
    pygame.draw.polygon(screen, GREEN, [[520, 130], [520, 490], [550, 490], [550, 130]])
    pygame.draw.line(screen, GREEN, [550, 340], [669, 270], 34)
    pygame.draw.polygon(screen, GREEN, [[560, 510], [590, 510], [590, 345], [560, 365]])
    pygame.draw.polygon(screen, GREEN, [[600, 510], [600, 340], [630, 350], [630, 510]])
    pygame.draw.rect(screen, GREEN, [640, 130, 30, 140])
    pygame.draw.rect(screen, GREEN, [640, 340, 30, 150])
    pygame.draw.line(screen, GREEN, [669, 340], [600, 310], 32)

    # Draws the name, "KYRIE IRVING"
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("KYRIE", True, PURPLE)
    screen.blit(text, [155, 300])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("KYRIE", True, SILVER)
    screen.blit(text, [930, 300])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("IRV", True, PURPLE)
    screen.blit(text, [527, 680])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("ING", True, SILVER)
    screen.blit(text, [600, 680])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("ING", True, SILVER)
    screen.blit(text, [600, 3])
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("IRV", True, PURPLE)
    screen.blit(text, [523, 3])

    pygame.display.flip()

    clock.tick(5)

pygame.quit()
