import pygame
import random

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_HEIGHT = 720
SCREEN_WIDTH = 1280
size = (SCREEN_WIDTH, SCREEN_HEIGHT)

screen = pygame.display.set_mode(size)

pygame.display.set_caption("Snow")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

snowList = []

NUMBER_OF_FLAKES = 500
FLAKE_MAX_SIZE = 7
FLAKE_MIN_SIZE = 2
FLAKE_MIN_SPEED = 1
FLAKE_MAX_SPEED = 5
PARTY = True

X = 0
Y = 1
SIZE = 2
SPEED = 3
COLOUR = 4

for i in range(NUMBER_OF_FLAKES):
    snowFlake = []

    x = random.randrange(SCREEN_WIDTH)
    snowFlake.append(x)

    y = random.randrange(SCREEN_HEIGHT)
    snowFlake.append(y)

    size = random.randrange(FLAKE_MIN_SIZE, FLAKE_MAX_SIZE + 1)
    snowFlake.append(size)

    speed = random.randrange(FLAKE_MIN_SPEED, FLAKE_MAX_SIZE + 1)
    snowFlake.append(speed)

    colour = WHITE

    if PARTY:
        colour = (random.randrange(256), random.randrange(256), random.randrange(256))
    snowFlake.append(colour)

    snowList.append(snowFlake)

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here

    for i in range(len(snowList)):
        pygame.draw.circle(screen, snowList[i][COLOUR], [snowList[i][X], snowList[i][Y]], 2)

        snowList[i][Y] += snowList[i][SPEED]

        if (snowList[i][Y] - snowList[i][SIZE] > SCREEN_HEIGHT):
            y = random.randrange(-50, -10)
            snowList[i][Y] = y

            x = random.randrange(SCREEN_WIDTH + 1)
            snowList[i][X] = x

            size = random.randrange(FLAKE_MIN_SIZE, FLAKE_MAX_SIZE + 1)
            snowList[i][SIZE] = size

            speed = random.randrange(FLAKE_MIN_SPEED, FLAKE_MAX_SPEED + 1)
            snowList[i][SPEED] = speed

            if PARTY:
                colour = (random.randrange(256), random.randrange(256), random.randrange(256))
                snowList[i][COLOUR] = colour

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()