import pygame
import random

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720
size = (1280, 720)

screen = pygame.display.set_mode(size)
rectX = 50
rectY = 50
rectSpeedX = 5
rectSpeedY = 5
rectHeight = 70
rectWidth = 70
rectColour = WHITE
RECT_MAX_HEIGHT = 1280
RECT_MAX_HEIGHT = 720
rectSizeChange = 50
rectSizeChange = 50

rectColour = (random.randrange(256), random.randrange(256), random.randrange(256))

pygame.display.set_caption("Bouncing Rectangle")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    rectX += rectSpeedX
    rectY += rectSpeedY

    if rectY > SCREEN_HEIGHT - rectHeight or rectY < 0:
        rectSpeedY *= -1
        rectColour = (random.randrange(256), random.randrange(256), random.randrange(256))
        rectWidth += 7
        if rectY > SCREEN_HEIGHT - RECT_MAX_HEIGHT:
            if rectY > SCREEN_HEIGHT - rectHeight:
                rectY - rectSizeChange

        #if rectSpeedY > 0:
            #rectSpeedY += 1
        #else:
            #rectSpeedY -= 1

    if rectX > SCREEN_WIDTH - rectWidth or rectX < 0:
        rectSpeedX *= -1
        rectColour = (random.randrange(256), random.randrange(256), random.randrange(256))
        rectHeight += 7

        #if rectSpeedX > 0:
            #rectSpeedX += 1
        #else:
            #rectSpeedX -= 1
    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here
    pygame.draw.rect(screen, rectColour, [rectX, rectY, rectWidth, rectHeight])
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
