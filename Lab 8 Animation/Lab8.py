# Janmarx Ybanez
# Lab 8
# Kyrie Irving Logo Animation
# CS20
# May 1

import pygame
import random

# Define some colors
GREEN = (0, 255, 0)
CHERRY_BLOSSOM = (255, 183, 197)
PURPLE = (158, 66, 244)
INDIGO = (75, 0, 130)
WATERMELON_PINK = (254, 127, 156)

pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

size = (SCREEN_WIDTH, SCREEN_HEIGHT)

screen = pygame.display.set_mode(size)

pygame.display.set_caption("Lab 8. Kyrie Irving Logo Animation")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Variables for The Snow Animation
snowList = []

NUMBER_OF_FLAKES = 500
FLAKE_MAX_SIZE = 7
FLAKE_MIN_SIZE = 2
FLAKE_MIN_SPEED = 1
FLAKE_MAX_SPEED = 5
PARTY = True

X = 0
Y = 1
SIZE = 2
SPEED = 3
COLOUR = 4

text_rotate_degrees = 0

# Creates Snowflakes
for i in range(NUMBER_OF_FLAKES):
    snowFlake = []

    x = random.randrange(SCREEN_WIDTH)
    snowFlake.append(x)

    y = random.randrange(SCREEN_HEIGHT)
    snowFlake.append(y)

    size = random.randrange(FLAKE_MIN_SIZE, FLAKE_MAX_SIZE + 1)
    snowFlake.append(size)

    speed = random.randrange(FLAKE_MIN_SPEED, FLAKE_MAX_SIZE + 1)
    snowFlake.append(speed)

    if PARTY:
        colour = (random.randrange(256), random.randrange(256), random.randrange(256))
    snowFlake.append(colour)

    snowList.append(snowFlake)

# Variables for Ball Animation
ballX = 100
ballY = 100
ballSpeedX = 5
ballSpeedY = 5

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # Background for my drawing
    screen.fill(INDIGO)

    # --- Drawing code should go here

    # Snow Animation
    for i in range(len(snowList)):
        pygame.draw.circle(screen, snowList[i][COLOUR], [snowList[i][X], snowList[i][Y]], 2)

        snowList[i][Y] += snowList[i][SPEED]

        if (snowList[i][Y] - snowList[i][SIZE] > SCREEN_HEIGHT):
            y = random.randrange(-50, -10)
            snowList[i][Y] = y

            x = random.randrange(SCREEN_WIDTH + 1)
            snowList[i][X] = x

            size = random.randrange(FLAKE_MIN_SIZE, FLAKE_MAX_SIZE + 1)
            snowList[i][SIZE] = size

            speed = random.randrange(FLAKE_MIN_SPEED, FLAKE_MAX_SPEED + 1)
            snowList[i][SPEED] = speed

            if PARTY:
                colour = (random.randrange(256), random.randrange(256), random.randrange(256))
                snowList[i][COLOUR] = colour

    # Draws out a circle and a polygon for like the background of Kyrie's logo
    pygame.draw.circle(screen, CHERRY_BLOSSOM, [600, 340], 330)
    pygame.draw.polygon(screen, PURPLE, [[400, 80], [800, 80], [800, 600], [400, 600]])

    # Draws a Ball
    pygame.draw.circle(screen, WATERMELON_PINK, [ballX, ballY], 100)

    # Ball Animation
    ballX += ballSpeedX
    ballY += ballSpeedY

    if ballY > SCREEN_HEIGHT or ballY < 0:
        ballSpeedY *= -1

    if ballX > SCREEN_WIDTH or ballX < 0:
        ballSpeedX *= -1

    # Draws the Kyrie Irving logo
    pygame.draw.polygon(screen, GREEN, [[560, 110], [560, 300], [590, 280], [590, 110]])
    pygame.draw.polygon(screen, GREEN, [[600, 110], [630, 110], [630, 260], [600, 280]])
    pygame.draw.polygon(screen, GREEN, [[520, 130], [520, 490], [550, 490], [550, 130]])
    pygame.draw.line(screen, GREEN, [550, 340], [669, 270], 34)
    pygame.draw.polygon(screen, GREEN, [[560, 510], [590, 510], [590, 345], [560, 365]])
    pygame.draw.polygon(screen, GREEN, [[600, 510], [600, 340], [630, 350], [630, 510]])
    pygame.draw.rect(screen, GREEN, [640, 130, 30, 140])
    pygame.draw.rect(screen, GREEN, [640, 340, 30, 150])
    pygame.draw.line(screen, GREEN, [669, 340], [600, 310], 32)

    # Draws the name, "KYRIE" and rotates
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("KYRIE", True, WATERMELON_PINK)
    text = pygame.transform.rotate(text, text_rotate_degrees)
    text_rotate_degrees += 4
    screen.blit(text, [155, 300])
    text = font.render("KYRIE", True, WATERMELON_PINK)
    text = pygame.transform.rotate(text, text_rotate_degrees)
    screen.blit(text, [930, 300])

    # Draws the name, "IRVING"
    text = font.render("IRVING", True, WATERMELON_PINK)
    screen.blit(text, [527, 680])
    text = font.render("IRVING", True, WATERMELON_PINK)
    screen.blit(text, [523, 3])

    # --- Updates the screen with what I've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
