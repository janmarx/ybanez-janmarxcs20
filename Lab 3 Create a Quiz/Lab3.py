# Lab 3: Make Your Own Quiz
# Janmarx Ybanez
# Computer Science 20
# Period 3
# 02/ 12/ 19

# Total answers correct stored in this variable
score = 0

# Question #1
print("Question #1")
print(" What is 7 x 8?")
answer = float(input(""))
# Answer feedback
if answer == 56:
    print("Correct")
    score = score + 1
else:
    print("Incorrect")
    score = 0
# prints the users' mark
print(" ", score, "/ 5\n")

# Question #2
print("Question #2")
print("Which country is not part of Europe?")
print("select A, or B, or C, or D")
# Options
print("A. Finland")
print("B. Turkey")
print("C. Croatia")
print("D. Canada")
answer = input("")
# Answer feedback
if answer.lower() == "d":
    print("Correct")
    score = score + 1
elif answer.lower() == "a":
    print("Incorrect")
    score = score + 0
elif answer.lower() == "b":
    print("Incorrect")
    score = score + 0
else:
    print("Incorrect")
    score = score + 0
# print the users' mark
print(" ", score, "/ 5\n")

# Question #3
print("Question #3")
print("Logan Lerman played the role Percy Jackson in the movie, Sea of Monsters?")
print("Select A or B")
# Options
print("A. True")
print("B. False")
answer = input("")
# Answer feedback
if answer.lower() == "a":
    print("correct")
    score = score + 1
else:
    print("Incorrect")
    score = score + 0
# Print the users' mark
print(" ", score, "/ 5\n")

# Question #4
print("Question #4")
print("What is the capital city of Germany?")
answer = input("")
# Answer feedback
if answer.lower() == "berlin":
    print("Correct")
    score = score + 1
else:
    print("Incorrect")
    score = score + 0
# Print users' mark
print(" ", score, "/ 5\n")

# Question #5
print("Question #5")
print("What is 100 + 100?")
answer = float(input(""))
# Answer feedback
if answer == 200:
    print("Correct")
    score = score + 1
else:
    print("Incorrect")
    score = score + 0
# Print the users' mark
print(" ", score, "/ 5\n")

# Total score and percentage
# Formula below calculates the percentage
percentage = score / 5 * 100
print("Your Total score:", score, "/ 5", "\nYour percentage:", percentage, "%")

# Feedback of how well the user did on the quiz
if percentage >= 70:
    print("Impressive!")
elif percentage <= 50:
    print("Great")
print("Done")
