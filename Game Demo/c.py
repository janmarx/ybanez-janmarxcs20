# List of settings for our game

# pipe variables

numberPipes = 10

maxPipeGap = 150
minPipeGap = 50

maxPipeSeparation = 600
minPipeSeparation = 200

maxPipeWidth = 100
minPipeWidth = 20

pipeSpeed = -5

# screen variables
screenWidth = 1280
screenHeight = 720

# player variables

PLAYER_COLOUR = (0, 0, 0)
playerX = 100
playerWidth = 40
playerHeight = 40
