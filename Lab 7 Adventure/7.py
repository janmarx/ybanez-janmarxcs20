# Janmarx Ybanez
# CS20
# 12/ 04 / 19
# Adventure Lab

key = 0
currentRoom = 0
roomList = []

# Introduction
print("You are a wealthy man and criminals abducted you as a hostage. They took you to their hideout\nand sent a video"
      " to your family demanding 100 million dollars from your bank account.If they\nlisten, the criminals will"
      " set you free. But, if your family does not listen and call the\npolice you will be killed!")

room = ["You are in a dark room with little insects crawling around. There is only a candle light\nhanging on the "
        "left wall of the room. On your right there is a human skeleton with cobwebs\non it. There is a passage "
        "to the north", 1, None, None, None]
roomList.append(room)

room = ["You are in a hallway. There is a passage to the east and west. There is also a exit north.\nFind a "
        "key to escape the hideout!", 1, 4, 2, 0]
roomList.append(room)

room = ["You are in the kitchen. There is a door to the north and east to go back in the hallway.", 3, 1, None, None]
roomList.append(room)

room = ["You are in a bedroom and found a key hanging on the wall. There is a person sleeping on\nthe bed. "
        "You quietly took the key. Go back to the hallway and unlock the door to escape!", None, None, 2, None]
roomList.append(room)

room = ["You are in a dungeon with weapons and torture devices all around the room. There is\na passage"
        "to the north and west to go back in the hallway.", 5, None, None, 1]
roomList.append(room)

room = ["You are in a bathroom. There is only a passage to the south, which is the dungeon.", None, None, 4, None]
roomList.append(room)

done = False
while not done:
    print("")
    print(roomList[currentRoom][0])
    print("")
    print("N.North E.East S.South W.West Q.Quit")
    userChoice = input("What's your move?")
    print("")

    # To Quit Game
    if userChoice.lower() == "q" or userChoice.lower() == "quit":
        done = True
        print("     You Lose")

    # If the player wants to go North
    if userChoice.lower() == "n" or userChoice.lower() == "north":
        nextRoom = roomList[currentRoom][1]
        currentRoom = nextRoom
        print(currentRoom)

    # If the player wants to go West
    elif userChoice.lower() == "w" or userChoice.lower() == "west":
        nextRoom = roomList[currentRoom][2]
        currentRoom = nextRoom
        print(currentRoom)

    # If the player wants to go East
    elif userChoice.lower() == "e" or userChoice.lower() == "east":
        nextRoom = roomList[currentRoom][4]
        currentRoom = nextRoom
        print(currentRoom)

    # if the player wants to go South
    elif userChoice.lower() == "s" or userChoice.lower() == "south":
        nextRoom = roomList[currentRoom][3]
        currentRoom = nextRoom
        print(currentRoom)

    # When the player reaches the bedroom he gets a key to escape
    if currentRoom is 3:
        key += 1
        done = True
        print("You found the key and escaped. You win!")

    # Tells player that the program doesn't understand what they typed
    else:
        print("Program does not understand what you typed")
