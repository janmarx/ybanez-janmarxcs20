# Janmarx Ybanez
# CS20
# 12/ 04 / 19
# Adventure Lab

# Variables
key = 0
currentRoom = 0
roomList = []

# Introduction
print("You are a wealthy man and criminals abducted you as a hostage. They took you to their hideout\nand sent a video"
      " to your family demanding 100 million dollars from your bank account.If they\nlisten, the criminals will"
      " set you free. But, if your family does not listen and call the\npolice you will be killed!")

# Starting Room 0
room = ["You are in a dark room with little insects crawling around. There is only a candle light\nhanging on the "
        "left wall of the room. On your right there is a human skeleton with cobwebs\non it. There is a passage "
        "to the north.", 1, None, None, None]
roomList.append(room)

# Hallway/Room 1
room = ["You are in a hallway. There is a passage to the east, west, and north.", 7, 2, 4, 0]
roomList.append(room)

# Kitchen/Room 2
room = ["You are in the kitchen. There is a door to the north and east to go back in the hallway.\n"
        "There is also a door west.", 3, 6, 1, None]
roomList.append(room)

# Bedroom/Room 3
room = ["You are in a bedroom and found a key hanging on the wall. There is a person sleeping on\nthe bed. "
        "You quietly took the key.", None, None, None, 2]
roomList.append(room)

# Dungeon/Room 4
room = ["You are in a dungeon with weapons and torture devices all around the room! There is\na passage"
        "to the north.", 5, 1, None, None]
roomList.append(room)

# Bathroom/Room 5
room = ["You are in a bathroom.", None, None, None, 4]
roomList.append(room)

# Storage/Room 6
room = ["Your in a storage room", None, None, 2, None]
roomList.append(room)

# Armory/Room 7
room = ["Your in the armory room. There's a balcony north", 8, None, None, 1]
roomList.append(room)

# Balcony/Room 8
room = ["Your on the balcony.", None, None, None, 7]
roomList.append(room)

# Variable to Quit
done = False
while not done:
    print("")
    print(roomList[currentRoom][0])
    print("")
    print("Instructions\nType:\nN. for North\nE. for East\nS. for South\nW. for West\nQ. for Quit")
    userChoice = input("What's your move?", )
    print("")

    # If the player wants to go North
    if userChoice.lower() == "n":
        nextRoom = roomList[currentRoom][1]
        if nextRoom == None:
            print("You can't go there.")
        else:
            currentRoom = nextRoom

    # If player wants to go West
    if userChoice.lower() == "w":
        nextRoom = roomList[currentRoom][2]
        if nextRoom == None:
            print("You can't go there.")
        else:
            currentRoom = nextRoom

    # If player wants to go East
    if userChoice.lower() == "e":
        nextRoom = roomList[currentRoom][3]
        if nextRoom == None:
            print("You can't go there.")
        else:
            currentRoom = nextRoom

    # If player wants to go South
    if userChoice.lower() == "s":
        nextRoom = roomList[currentRoom][4]
        if nextRoom == None:
            print("You can't go there.")
        else:
            currentRoom = nextRoom

    # To Quit Game
    if userChoice.lower() == "q":
        done = True
        print("     You Quit Game")

    # Found a key to escape
    if currentRoom == 3:
        key = 1
        print("You found the key and escaped. The criminals were arrested.\n        You Win")
        done = True
