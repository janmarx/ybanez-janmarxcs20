# Vinay Sabu, Janmarx Ybanez
# SC20
# List Exercises Odds and evens
# 3/25/2019

# list 1
list1 = [2, 4, 6, 8, 10, 12, 14]
# even an odd lists adds even and odd numbers to the list
even = []
odd = []
# adds even odd #s to list
for i in list1:
    if i % 2 == 0:
        even.append(i)
    else:
        odd.append(i)
print("odd numbers - ", odd)
print("even numbers - ", even)
print()
# list 2
list2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# even an odd lists adds even and odd numbers to the list
even = []
odd = []
# adds even odd #s to list
for i in list2:
    if i % 2 == 0:
        even.append(i)
    else:
        odd.append(i)
print("odd numbers - ", odd)
print("even numbers - ", even)
print()

# list 3
list3 = [2, 10, 20, 21, 23, 24, 40, 55, 60, 61]
# even an odd lists adds even and odd numbers to the list
even = []
odd = []
# adds even odd #s to list
for i in list3:
    if i % 2 == 0:
        even.append(i)
    else:
        odd.append(i)
print("odd numbers - ", odd)
print("even numbers - ", even)
