myList = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8, 7]
# 26 #s in list
print()
print(myList)
print()
# problem # 1
# * Finds the sum of spots 3 – 16 = 76

listTotal = 0
for item in range(len(myList[3:16])):
    listTotal += myList[item]
print("sum of spots 3-16 = ", listTotal)
print()

# problem # 2
listTotal = 0
for item in range(len(myList[2:11])):
    listTotal += myList[item]
print("sum of spots 2-9 = ", listTotal)
print()

# problem # 3
totalZero = 0
for item in myList:
    if item == 0:
        totalZero += 1
print("# of 0s = ", totalZero)
# problem # 4
totalThree = 0
for item in myList:
    if item == 3:
        totalThree += 1
print("# of 3s = ", totalThree)
# problem # 5
totalSevens = 0
for item in myList:
    if item == 7:
        totalSevens += 1
print("# of 7s = ", totalSevens)
print()

# problem # 6

# Vinay Sabu, Janmarx Ybanez
# SC20
# List Exercises List FunHouse
# 3/25/2019

myList = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8, 7]

print()
print(myList)
print()

# problem # 1
# * Finds the sum of spots 3 – 16 = 76
listTotal = 0

# Got help from Lance for line 10
for item in range(3, 17):
    listTotal += myList[item]
print("sum of spots 3-16 = ", listTotal)
print()

# problem # 2
listTotal = 0
# got help from Lance for line 17
for item in range(2, 10):
    listTotal += myList[item]
print("sum of spots 2-9 = ", listTotal)
print()

# problem # 3
totalZero = 0
for item in myList:
    if item == 0:
        totalZero += 1
print("# of 0s = ", totalZero)
# problem # 4
totalThree = 0
for item in myList:
    if item == 3:
        totalThree += 1
print("# of 3s = ", totalThree)
# problem # 5
totalSevens = 0
for item in myList:
    if item == 7:
        totalSevens += 1
print("# of 7s = ", totalSevens)
print()

# problem # 6
sevenList = []
noSeven = []
for i in myList:
    if i == 7:
        sevenList.append(i)
    else:
        noSeven.append(i)
print("new list with all 7s removed = ", noSeven)
totalSevens = 0
for item in noSeven:
    if item == 7:
        totalSevens += 1
print("# of 7s = ", totalSevens)

