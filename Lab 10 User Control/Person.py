"""
Example of turning a drawing into a function
Drawing by Paige Lekach
Simplified by Mr. Birrell
"""

import pygame
import math

# Colours

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
SKY_BLUE = (79, 200, 240)
GRASS_GREEN = (12, 145, 63)
BROWN = (153, 102, 0)
KITE_PINK = (255, 153, 255)
KITE_PURPLE = (204, 0, 204)
SUN_YELLOW = (255, 255, 102)
PEACH = (255, 255, 230)
BRIGHT_PINK = (255, 51, 153)
LIGHT_BROWN = (204, 153, 0)
BASKET_BROWN = (102, 51, 0)
BLANKET_RED = (179, 0, 0)
BUSH_GREEN = (27, 125, 29)
PI = 3.141592653

pygame.init()

size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Create-a-picture")

done = False

clock = pygame.time.Clock()


def drawPerson(screen, x, y):
    pygame.draw.ellipse(screen, PEACH, [450 + x - 425, 200 + y - 200, 45, 40])
    pygame.draw.ellipse(screen, BLACK, [455 + x - 425, 215 + y - 200, 5, 5])
    pygame.draw.arc(screen, GRASS_GREEN, [445 + x - 425, 212 + y - 200, 20, 20], PI / 2, PI, 2)
    pygame.draw.arc(screen, PEACH, [445 + x - 425, 212 + y - 200, 20, 20], 0, PI / 2, 2)
    pygame.draw.arc(screen, BLACK, [445 + x - 425, 212 + y - 200, 20, 20], 3 * PI / 2, 2 * PI, 2)
    pygame.draw.arc(screen, GRASS_GREEN, [445 + x - 425, 212 + y - 200, 20, 20], PI, 3 * PI / 2, 2)
    pygame.draw.rect(screen, PEACH, [467 + x - 425, 235 + y - 200, 10, 9])
    pygame.draw.rect(screen, BLUE, [448 + x - 425, 242 + y - 200, 50, 50])
    pygame.draw.rect(screen, BLUE, [435 + x - 425, 255 + y - 200, 17, 9])
    pygame.draw.ellipse(screen, PEACH, [425 + x - 425, 255 + y - 200, 10, 10])
    pygame.draw.rect(screen, BLUE, [435 + x - 425, 267 + y - 200, 17, 9])
    pygame.draw.ellipse(screen, PEACH, [425 + x - 425, 267 + y - 200, 10, 10])
    pygame.draw.rect(screen, BLACK, [448 + x - 425, 290 + y - 200, 50, 35])
    pygame.draw.rect(screen, BLACK, [442 + x - 425, 315 + y - 200, 20, 10])
    pygame.draw.rect(screen, SUN_YELLOW, [440 + x - 425, 198 + y - 200, 60, 10])
    pygame.draw.rect(screen, SUN_YELLOW, [452 + x - 425, 185 + y - 200, 40, 20])


pygame.mouse.set_visible(False)

xSpeed = 0
ySpeed = 0

personKeyboardX = 0
personKeyboardY = 0

MAX_X = 700
MAX_Y = 500

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                xSpeed = -3
            elif event.key == pygame.K_RIGHT:
                xSpeed = 3
            elif event.key == pygame.K_UP:
                ySpeed = -3
            elif event.key == pygame.K_DOWN:
                ySpeed = 3
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or pygame.K_RIGHT or pygame.K_UP or pygame.K_DOWN:
                xSpeed = 0
                ySpeed = 0

    screen.fill(GRASS_GREEN)

    # Game Logic
    mousePosition = pygame.mouse.get_pos()
    personMouseX = mousePosition[0]
    personMouseY = mousePosition[1]

    personKeyboardX += xSpeed
    personKeyboardY += ySpeed

    if personKeyboardY >= MAX_Y:
        personKeyboardY *= -1

    if personKeyboardX >= MAX_X:
        personKeyboardX *= -1

    # Sky
    pygame.draw.rect(screen, SKY_BLUE, [0, 0, 700, 240])

    # Hill
    pygame.draw.ellipse(screen, GRASS_GREEN, [-5, 120, 500, 300])

    # Sun
    pygame.draw.ellipse(screen, SUN_YELLOW, [-14, -10, 85, 80])
    pygame.draw.line(screen, SUN_YELLOW, [0, 50], [10, 140], 5)
    pygame.draw.line(screen, SUN_YELLOW, [40, 65], [70, 140], 5)
    pygame.draw.line(screen, SUN_YELLOW, [60, 40], [120, 100], 6)
    pygame.draw.line(screen, SUN_YELLOW, [60, 20], [150, 30], 5)

    # Clouds
    pygame.draw.ellipse(screen, WHITE, [470, 20, 90, 80])
    pygame.draw.ellipse(screen, WHITE, [530, 35, 90, 80])
    pygame.draw.ellipse(screen, WHITE, [490, 60, 90, 80])

    # Person
    drawPerson(screen, 0, 0)
    drawPerson(screen, personMouseX - 50, personMouseY)
    drawPerson(screen, personKeyboardX, personKeyboardY)

    pygame.display.flip()

    clock.tick(60)

pygame.quit()