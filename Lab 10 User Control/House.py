"""
Example for turning a drawing into a function
House by Ben Camplin
"""

import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
TURQUOISE = (24, 201, 178)
ORANGE = (255, 136, 0)
PURPLE = (173, 0, 204)
GREY = (222, 222, 222)
YELLOW = (250, 250, 0)
LIGHTBLUE = (145, 228, 255)
DARKGREEN = (0, 156, 60)

pygame.init()

# Set the width and height of the screen [width, height]
size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()


def drawHouse(screen, x, y):
    # The house
    pygame.draw.rect(screen, ORANGE, [100 + x - 100, 400 + y - 200, 101, -100], 0)

    pygame.draw.polygon(screen, RED, [[150 + x - 100, 200 + y - 200], [100 + x - 100, 298 + y - 200], [200 + x - 100,
                                                                                                       298 + y - 200]])

    # Window
    pygame.draw.rect(screen, WHITE, [110 + x - 100, 335 + y - 200, 20, -20])

    # Door
    pygame.draw.rect(screen, BLACK, [150 + x - 100, 400 + y - 200, 20, -50])


pygame.mouse.set_visible(False)

xSpeed = 0
ySpeed = 0

houseKeyboardX = 0
houseKeyboardY = 0

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                xSpeed = -3
            elif event.key == pygame.K_RIGHT:
                xSpeed = 3
            elif event.key == pygame.K_UP:
                ySpeed = -3
            elif event.key == pygame.K_DOWN:
                ySpeed = 3
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or pygame.K_RIGHT or pygame.K_UP or pygame.K_DOWN:
                xSpeed = 0
                ySpeed = 0

    # --- Game logic should go here
    mousePosition = pygame.mouse.get_pos()
    houseMouseX = mousePosition[0]
    houseMouseY = mousePosition[1]

    houseKeyboardX += xSpeed
    houseKeyboardY += ySpeed
    # --- Drawing code should go here

    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(WHITE)

    # drawHouse(screen, 0, 0)
    drawHouse(screen, houseMouseX - 50, houseMouseY)
    drawHouse(screen, houseKeyboardX, houseKeyboardY)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()