# Janmarx Ybanez
# Lab 10
# Kyrie Irving Logo Animation/Control 2 Objects
# CS20

import pygame
import random

# Define some colors
GREEN = (0, 255, 0)
CHERRY_BLOSSOM = (255, 183, 197)
PURPLE = (158, 66, 244)
INDIGO = (75, 0, 130)
WATERMELON_PINK = (254, 127, 156)
ORANGE = (255, 140, 0)
BLACK = (0, 0, 0)

PI = 3.14159265359

pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

size = (SCREEN_WIDTH, SCREEN_HEIGHT)

screen = pygame.display.set_mode(size)

pygame.display.set_caption("Lab 8. Kyrie Irving Logo Animation/Control 2 objects")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Variables for The Snow Animation
snowList = []

NUMBER_OF_FLAKES = 500
FLAKE_MAX_SIZE = 7
FLAKE_MIN_SIZE = 2
FLAKE_MIN_SPEED = 1
FLAKE_MAX_SPEED = 5
PARTY = True

X = 0
Y = 1
SIZE = 2
SPEED = 3
COLOUR = 4


# Creates Snowflakes
for i in range(NUMBER_OF_FLAKES):
    snowFlake = []

    x = random.randrange(SCREEN_WIDTH)
    snowFlake.append(x)

    y = random.randrange(SCREEN_HEIGHT)
    snowFlake.append(y)

    size = random.randrange(FLAKE_MIN_SIZE, FLAKE_MAX_SIZE + 1)
    snowFlake.append(size)

    speed = random.randrange(FLAKE_MIN_SPEED, FLAKE_MAX_SIZE + 1)
    snowFlake.append(speed)

    if PARTY:
        colour = (random.randrange(256), random.randrange(256), random.randrange(256))
    snowFlake.append(colour)

    snowList.append(snowFlake)

# Function for drawing a basketball
def draw_basketball(screen, x, y):
    pygame.draw.circle(screen, ORANGE, [100 + x - 0, 100 + y - 19], 100)
    pygame.draw.line(screen, BLACK, [97 + x - 0, 0 + y - 19], [97 + x - 0, 200 + y - 19], 5)
    pygame.draw.line(screen, BLACK, [0 + x - 0, 100 + y - 19], [200 + x - 0, 100 + y - 19], 5)
    pygame.draw.arc(screen, BLACK, [123 + x - 0, 26 + y - 19, 100, 346], PI / 2, PI, 5)
    pygame.draw.arc(screen, BLACK, [15 + x - 0, 19 + y - 19, 55, 345], 0, PI / 2, 5)


# Function for drawing Kyrie Irving's logo
def draw_kyrie_logo(screen, x, y):
    pygame.draw.polygon(screen, GREEN, [[560 + x - 520, 110 + y - 110], [560 + x - 520, 300 + y - 110], [590 + x - 520,
                                                                        280 + y - 110], [590 + x - 520, 110 + y - 110]])
    pygame.draw.polygon(screen, GREEN, [[600 + x - 520, 110 + y - 110], [630 + x - 520, 110 + y - 110], [630 + x - 520,
                                                                        260 + y - 110], [600 + x - 520, 280 + y - 110]])
    pygame.draw.polygon(screen, GREEN, [[520 + x - 520, 130 + y - 110], [520 + x - 520, 490 + y - 110], [550 + x - 520,
                                                                        490 + y - 110], [550 + x - 520, 130 + y - 110]])
    pygame.draw.line(screen, GREEN, [550 + x - 520, 340 + y - 110], [669 + x - 520, 270 + y - 110], 34)
    pygame.draw.polygon(screen, GREEN, [[560 + x - 520, 510 + y - 110], [590 + x - 520, 510 + y - 110], [590 + x - 520,
                                                                        345 + y - 110], [560 + x - 520, 365 + y - 110]])
    pygame.draw.polygon(screen, GREEN, [[600 + x - 520, 510 + y - 110], [600 + x - 520, 340 + y - 110], [630 + x - 520,
                                                                        350 + y - 110], [630 + x - 520, 510 + y - 110]])
    pygame.draw.rect(screen, GREEN, [640 + x - 520, 130 + y - 110, 30, 140])
    pygame.draw.rect(screen, GREEN, [640 + x - 520, 340 + y - 110, 30, 150])
    pygame.draw.line(screen, GREEN, [669 + x - 520, 340 + y - 110], [600 + x - 520, 310 + y - 110], 32)


# Does not show the mouse
pygame.mouse.set_visible(True)

# sets X and Y speed to zero
xSpeed = 0
ySpeed = 0
kyrieKeyboardX = 0
kyrieKeyboardY = 0

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                xSpeed = - 5
            elif event.key == pygame.K_RIGHT:
                xSpeed = 5
            elif event.key == pygame.K_UP:
                ySpeed = - 5
            elif event.key == pygame.K_DOWN:
                ySpeed = 5
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or pygame.K_RIGHT or pygame.K_UP or pygame.K_DOWN:
                xSpeed = 0
                ySpeed = 0

    # Background for my drawing
    screen.fill(INDIGO)

    # Game Logic
    mousePosition = pygame.mouse.get_pos()
    ballMouseX = mousePosition[0]
    ballMouseY = mousePosition[1]

    kyrieKeyboardX += xSpeed
    kyrieKeyboardY += ySpeed

    # --- Drawing code should go here
    # Snow Animation
    for i in range(len(snowList)):
        pygame.draw.circle(screen, snowList[i][COLOUR], [snowList[i][X], snowList[i][Y]], 2)

        snowList[i][Y] += snowList[i][SPEED]

        if (snowList[i][Y] - snowList[i][SIZE] > SCREEN_HEIGHT):
            y = random.randrange(-50, -10)
            snowList[i][Y] = y

            x = random.randrange(SCREEN_WIDTH + 1)
            snowList[i][X] = x

            size = random.randrange(FLAKE_MIN_SIZE, FLAKE_MAX_SIZE + 1)
            snowList[i][SIZE] = size

            speed = random.randrange(FLAKE_MIN_SPEED, FLAKE_MAX_SPEED + 1)
            snowList[i][SPEED] = speed

            if PARTY:
                colour = (random.randrange(256), random.randrange(256), random.randrange(256))
                snowList[i][COLOUR] = colour

    # Creates boundaries to prevent objects from leaving the screen
    if kyrieKeyboardX >= 610:
        kyrieKeyboardX = 610
    if kyrieKeyboardX <= -525:
        kyrieKeyboardX = -525
    if kyrieKeyboardY <= -115:
        kyrieKeyboardY = -115
    if kyrieKeyboardY >= 207:
        kyrieKeyboardY = 207
    if ballMouseX < 90:
        ballMouseX = 90
    if ballMouseX > 1175:
        ballMouseX = 1175
    if ballMouseY < 99:
        ballMouseY = 99
    if ballMouseY > 625:
        ballMouseY = 625

    # Draws out a circle and a polygon for like the background of Kyrie's logo
    pygame.draw.circle(screen, CHERRY_BLOSSOM, [600, 340], 330)
    pygame.draw.polygon(screen, PURPLE, [[400, 80], [800, 80], [800, 600], [400, 600]])

    # Draws the Kyrie Irving logo
    pygame.draw.polygon(screen, GREEN, [[560, 110], [560, 300], [590, 280], [590, 110]])
    pygame.draw.polygon(screen, GREEN, [[600, 110], [630, 110], [630, 260], [600, 280]])
    pygame.draw.polygon(screen, GREEN, [[520, 130], [520, 490], [550, 490], [550, 130]])
    pygame.draw.line(screen, GREEN, [550, 340], [669, 270], 34)
    pygame.draw.polygon(screen, GREEN, [[560, 510], [590, 510], [590, 345], [560, 365]])
    pygame.draw.polygon(screen, GREEN, [[600, 510], [600, 340], [630, 350], [630, 510]])
    pygame.draw.rect(screen, GREEN, [640, 130, 30, 140])
    pygame.draw.rect(screen, GREEN, [640, 340, 30, 150])
    pygame.draw.line(screen, GREEN, [669, 340], [600, 310], 32)

    # Draws the name, "KYRIE" and rotates
    font = pygame.font.SysFont('Calibri', 50, True, False)
    text = font.render("KYRIE", True, WATERMELON_PINK)
    screen.blit(text, [155, 300])
    text = font.render("KYRIE", True, WATERMELON_PINK)
    screen.blit(text, [930, 300])

    # Draws the name, "IRVING"
    text = font.render("IRVING", True, WATERMELON_PINK)
    screen.blit(text, [527, 680])
    text = font.render("IRVING", True, WATERMELON_PINK)
    screen.blit(text, [523, 3])

    # Basketball and you can control it using a mouse
    draw_basketball(screen, ballMouseX - 90, ballMouseY - 80)

    # draws a basketball at the top left corner of screen
    draw_basketball(screen, 0, 20)

    # Kyrie logo and you can control it using keyboard
    draw_kyrie_logo(screen, kyrieKeyboardX + 521, kyrieKeyboardY + 110)

    # --- Updates the screen with what I've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
