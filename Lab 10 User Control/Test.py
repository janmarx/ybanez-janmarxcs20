import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Test")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()


def draw_basketball(screen, x, y):
    pygame.draw.circle(screen, BLACK, [300 + x - 100, 300 + y - 100], 100)


def draw_kyrie_logo(screen, x, y):
    pygame.draw.polygon(screen, GREEN, [[560 + x - 520, 110 + y - 110], [560 + x - 520, 300 + y - 110], [590 + x - 520,
                                                                        280 + y - 110], [590 + x - 520, 110 + y - 110]])
    pygame.draw.polygon(screen, GREEN, [[600 + x - 520, 110 + y - 110], [630 + x - 520, 110 + y - 110], [630 + x - 520,
                                                                        260 + y - 110], [600 + x - 520, 280 + y - 110]])
    pygame.draw.polygon(screen, GREEN, [[520 + x - 520, 130 + y - 110], [520 + x - 520, 490 + y - 110], [550 + x - 520,
                                                                        490 + y - 110], [550 + x - 520, 130 + y - 110]])
    pygame.draw.line(screen, GREEN, [550 + x - 520, 340 + y - 110], [669 + x - 520, 270 + y - 110], 34)
    pygame.draw.polygon(screen, GREEN, [[560 + x - 520, 510 + y - 110], [590 + x - 520, 510 + y - 110], [590 + x - 520,
                                                                        345 + y - 110], [560 + x - 520, 365 + y - 110]])
    pygame.draw.polygon(screen, GREEN, [[600 + x - 520, 510 + y - 110], [600 + x - 520, 340 + y - 110], [630 + x - 520,
                                                                        350 + y - 110], [630 + x - 520, 510 + y - 110]])
    pygame.draw.rect(screen, GREEN, [640 + x - 520, 130 + y - 110, 30, 140])
    pygame.draw.rect(screen, GREEN, [640 + x - 520, 340 + y - 110, 30, 150])
    pygame.draw.line(screen, GREEN, [669 + x - 520, 340 + y - 110], [600 + x - 520, 310 + y - 110], 32)


pygame.mouse.set_visible(False)

xSpeed = 0
ySpeed = 0
kyrieKeyboardX = 0
kyrieKeyboardY = 0

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                xSpeed = -3
            elif event.key == pygame.K_RIGHT:
                xSpeed = 3
            elif event.key == pygame.K_UP:
                ySpeed = -3
            elif event.key == pygame.K_DOWN:
                ySpeed = 3
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or pygame.K_RIGHT or pygame.K_UP or pygame.K_DOWN:
                xSpeed = 0
                ySpeed = 0

    # --- Game logic should go here
    mousePosition = pygame.mouse.get_pos()
    ballMouseX = mousePosition[0]
    ballMouseY = mousePosition[1]

    kyrieKeyboardX += xSpeed
    kyrieKeyboardY += ySpeed

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code should go here
    draw_basketball(screen, ballMouseX - 50, ballMouseY)
    draw_kyrie_logo(screen, kyrieKeyboardX, kyrieKeyboardY)

    if kyrieKeyboardX >= 1134:
        kyrieKeyboardX = 1134

    if kyrieKeyboardX <= 0:
        kyrieKeyboardX = 0

    if kyrieKeyboardY <= 0:
        kyrieKeyboardY = 0

    if kyrieKeyboardY >= 317:
        kyrieKeyboardY = 317


    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
